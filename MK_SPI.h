/*
 * SPI library file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : ATmega328p SPI control
 */ 

#ifndef MK_SPI_H_
#define MK_SPI_H_

#include "MK_utilities.h"
#include <avr/common.h>



/**
 * Initializes the SPI interface.
 *
 * Set I/Os direction, Set master, clock rate and activate
*/
void SPIinit(void);


/**
 * Send byte and wait for finishing transmission 
 *
 * @param dataByte data to send
*/
void SPIwriteByte(uint8_t dataByte);


/**
 * Read one byte send through SPI - requires NULL byte to provide clock for slave
 *
 * @return incoming byte
*/
uint8_t SPIreadByte(void);


/**
 * One function to read and write because of similar construction both operations.
 *
 * @param dataByte data to send, of NULL to read
 * @return incoming byte
*/
uint8_t SPIreadWrite(uint8_t dataByte);


/**
 * Read particular register from device
 *
 * @param regID register ID
 * @param *dataByte buffer for incomming values
 * @param dataSize amount of incoming bytes to read
*/
void SPIgetReg(uint8_t regID, uint8_t *dataByte, uint8_t dataSize);


/**
 * Write bytes into selected register
 *
 * @param regID register ID
 * @param dataByte data to send
 * @param dataSize amount of bytes to send
*/
void SPIwriteReg(uint8_t regID, uint8_t *dataByte, uint8_t dataSize);

#endif /* MK_SPI_H_ */
