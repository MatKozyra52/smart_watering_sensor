# System podlewania roślin bazujący na informacji z internetowego serwisu pogodowego
Niniejsze repozytorium stanowi uzupełnienie projektu dyplomowego. 

## Pliki programu

### Biblioteki zewnętrzne:
 - **nrf24l01-mnemonics.h** – Zbiór definicji opisujących adresy rejestrów modułu nRF24L01.

### Biblioteki dedykowane
 - **MK_ADC** – obsługa przetwornika dostępnego w mikrokontrolerze ATmega, odczyt pomiarów z czujnika wilgotności gleby. 
 - **MK_easyUART** – podstawowa obsługa magistrali UART służąca wyłącznie do monitorowania działania kodu. 
 - **MK_INFO** – część logiczna zawierająca maszynę stanów, oraz przypisane im akcje. Najbardziej rozbudowany plik spajający pozostałe biblioteki. 
 - **MK_NRF24L01** – własna biblioteka do obsługi modułu nRF24L01. 
 - **MK_SLEEP** – obsługa trybów uśpienia. 
 - **MK_SPI** – konfiguracja i obsługa magistrali SPI. 
 - **MK_utilities.h** – zbiór powszechnie wykorzystywanych funkcji, np. operacje bitowe. 
 - **main.c** – główny kod programu. Inicjuje peryferia takie jak UART, ADC. Uruchamia maszynę stanów. Stanowi również definicje dla funkcji przerwań. 
 
 

