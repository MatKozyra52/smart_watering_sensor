/*
 * MAIN file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : Remote sensor - main code
 */ 


#include "MK_utilities.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <avr/wdt.h>
#include <avr/sleep.h>

// Debbug
#include "MK_easyUART.h"
// Periferia
#include "MK_ADC.h"
#include "MK_DHT22.h"
// Communication
#include "MK_NRF24L01.h"
#include "MK_SPI.h"
#include "MK_INFO.h"

#include "MK_sleep.h"

// Set up UART for printf();
#ifndef BAUD
#define BAUD 9600
#endif

#define  BUFF_SIZE 32
uint8_t tx_message[BUFF_SIZE];				// Define string array
uint8_t rx_message[BUFF_SIZE];				// Define string array
DATA_MACHINE stateMachine;

void IRQ_pinChangeInit();

int main(void)
{
	strcpy((char*)(tx_message),"Hello World!");	// Copy string into array
	
	//	Initialize UART
	initeasyUART();
	//ADC
	ADC_init();
	
	MACHINE_initMachine(&stateMachine);
	
	NRF_printDetails();
	
	IRQ_pinChangeInit();
	printf("Start the loop\n\r");
	while (1)
	{
		MACHINE_stateMachine(&stateMachine);
	}
}


//	Interrupt on IRQ pin - Incoming data
ISR(INT0_vect)
{
	uint8_t status = NRF_getStatus();
	printf("\tIRQ\n\r");
	if(stateMachine.newRx == 0){
		while (!NRF_emptyRXbuff()){
			//READ DATA
			memset(rx_message, 0, BUFF_SIZE);
			size_t len = NRF_readMessage(rx_message);
			DATA_PACKET buff;
			DATA_rawToPacket(rx_message, len, &buff);
			if(buff.node_id == 1) {
				memcpy(&stateMachine.packetBuffer, &buff, sizeof(DATA_PACKET));
			}
			else(printf("Packet drop\n\r"));
			printf("Status: %X,\t Message: %s \tCMD: %X\n\r",status, (char*)stateMachine.packetBuffer.payload, stateMachine.packetBuffer.header.cmd_id);			
		}
	}
	stateMachine.newRx = 1;
	NRF_clearInterrupt(1,0,0);
}

// Watchdog interrupt
ISR(WDT_vect){
	printf("WDT IRQ");
}

// Configuration reset
ISR(PCINT2_vect)
{
	printf("pcint2\n\n");
	MACHINE_clearConfig();
	Reset_AVR();
}

// Configuration reset - IRQ init
void IRQ_pinChangeInit(){
	cli();
	PIND = 1<<7;			// D7 - 7(Arduino)
	PCICR |= 0b00000111;	// Enables Ports B and C Pin Change Interrupts
	PCMSK2 |= 0b10000000;	// PCINT23
	sei();	
}