/*
 * State machine library file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : Packet handling, State machine of the device,
 */ 

#ifndef MK_INFO_H_
#define MK_INFO_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "MK_NRF24L01.h"
#include "MK_sleep.h"
#include "MK_utilities.h"
#include "time.h"

#define DEBUG 1

#define OVERHEAD_LEN 	2
#define HEADER_REQ		7
#define HEADER_ACTION	4
#define HEADER_CMD		0
#define HEADER_CMD_MASK	0x0F

/**** CMD ********************/
#define CMD_PING		0b0001
#define CMD_SET_RX_ADDR	0b0010
#define CMD_SET_TX_ADDR	0b0011
#define CMD_GET_DATA	0b0100
#define CMD_SET_NODE_ID	0b0101
#define CMD_INIT		0b1110
#define CMD_GO_TO_SLEEP	0b1111
/**** CMD ********************/
#define CMD_REQUEST		1
#define CMD_RESPONSE	0

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _DATA_HEADER{
	bool request;					// 7 - Request/Response
									// 6:5 - Not used yet
	bool action;					// 4 - Action/Information
	uint8_t cmd_id;					// 3:0 - Command ID
	} DATA_HEADER;

typedef struct _DATA_PACKET{
	uint8_t node_id;
	DATA_HEADER header;
	uint8_t payload[30];
	} DATA_PACKET;

/* Sensor values struct */
typedef struct _DATA_VALUES{
	uint16_t temperature;			// for future developent
	uint16_t humidity;				// for future developent
	uint16_t moisture;
	}DATA_VALUES;

/* States of machine */
typedef enum {
	S_PACKET_HANDLE,				// wait for sleep or other cmd
	S_SLEEP,						// periferia off
	S_HELLO,						// after sleep - send ping
	S_FIRST
	}MACHINE_STATE;
	
	
	
/* Struct for local machine of states */
typedef struct _DATA_MACHINE{
	bool newRx;						// new packet indicator
	DATA_PACKET packetBuffer;		// last packet
	MACHINE_STATE state;		// actual machine state
}DATA_MACHINE;

/******************************************************************
 * Radio communication - Packet handling 
******************************************************************/
bool DATA_action(DATA_PACKET* packet);
bool DATA_actionInit(DATA_PACKET* packet);
bool DATA_actionPing(DATA_PACKET* packet);
bool DATA_actionGetData(DATA_PACKET* packet);
bool DATA_actionSetTxAddr(DATA_PACKET* packet);
bool DATA_actionSetNodeId(DATA_PACKET* packet);
bool DATA_actionGoToSleep(DATA_PACKET* packet);
bool DATA_rawToPacket(uint8_t* raw_data, size_t len, DATA_PACKET* packet);
size_t DATA_packetToRaw(struct _DATA_PACKET* packet, uint8_t** raw_data);
NRF_TX_RESULT DATA_sendPing();
void DATA_updateSensor();
void DATA_printPacket(DATA_PACKET* packet);
void DATA_createPacket(DATA_PACKET* packet, bool act, uint8_t cmd, bool request);
void DATA_createPacketRequest(DATA_PACKET* packet, bool act, uint8_t cmd);
void DATA_createPacketResponse(DATA_PACKET* packet, bool act, uint8_t cmd);
/*****************************************************************/


/******************************************************************
 * State machine procedures
******************************************************************/
void MACHINE_loadConfig();
void MACHINE_saveConfig();
void MACHINE_clearConfig();
void MACHINE_initMachine(DATA_MACHINE *stateMachine);
void MACHINE_stateMachine(DATA_MACHINE *stateMachine);
void MACHINE_changeState(MACHINE_STATE setState, DATA_MACHINE *stateMachine);
/*****************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* MK_INFO_H_ */