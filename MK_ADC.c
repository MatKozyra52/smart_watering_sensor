
#include "MK_ADC.h"
#include "MK_utilities.h"
#include <stdlib.h>

#define ADC_START_CONVERSION SETBIT(ADCSRA, ADSC);
void ADC_init(){
	
	/*Turn on ADC*/
	CLEARBIT(PRR, PRADC);
	
	
	/*
	REFS - Reference voltage
	ADLAR - Data side adjust
	MUX - ADC multiplexed input
	*/
	ADMUX	|=	0 << REFS1 | 1 << REFS0
			|	0 << ADLAR
			|	0 << MUX3 | 0 << MUX2 | 0 << MUX1 | 0 << MUX0;
		
	/*
	ADEN - Enable ADC
	ADSC - Start conversion
	ADATE - Auto Trigger
	ADIF - Interrupt Flag
	ADIE - Interrupt Enable
	ADPS - ADC internal clock prescaler
	*/	
	ADCSRA	|=	1 << ADEN 
			|	0 << ADSC 
			|	0 << ADATE 
			|	0 << ADIF | 0 << ADIE 
			|	1 << ADPS2 | 1 << ADPS1 | 1 << ADPS0;
			
	
	/*
	ADTS - choose ADC conversion activation  signal
	*/		
	ADCSRB	|=	0 << ADTS2 | 0 << ADTS1 | 0 << ADTS0;		
	
}

uint16_t ADC_readRaw(uint8_t channel){
	
	ADMUX |= (channel & 0x0F);
	ADC_START_CONVERSION;
	while (ADCSRA & 1<<ADSC){}
	
	return ADC;
}


uint16_t ADC_getValue(uint8_t channel, uint8_t samples){
	uint16_t result = 0;
	uint16_t diff = 0;
	uint16_t sample1, sample2;
	
	// Check if values are similar
	do{
		sample1 = ADC_readRaw(channel);
		_delay_ms(2);
		sample2 = ADC_readRaw(channel);
		diff = abs(sample1 - sample2);
	} while (diff > 200);
	result = (sample1+sample2)/2;
	
	for(uint8_t sample = 1; sample < samples; sample ++){
		sample1 = ADC_readRaw(channel);
		result += sample1;
	}
	return (result/samples);
}


void ADC_sleep(){
	CLEARBIT(ADCSRA, ADEN);
	ADCSRA = 0;
	SETBIT(PRR, PRADC);
	
	CLEARBIT(SENSOR_POWER_PORT, SENSOR_POWER_POS);
}


void ADC_wake(){
	SETBIT(SENSOR_POWER_DDR, SENSOR_POWER_POS);				// enable sensor module
	SETBIT(SENSOR_POWER_PORT, SENSOR_POWER_POS);
	
	CLEARBIT(PRR, PRADC);
	ADC_init();
}