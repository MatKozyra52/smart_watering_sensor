/*
 * Library file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : Binary operations
 */ 

#ifndef MK_UTILITIES_H_
#define MK_UTILITIES_H_

#include <avr/io.h>
#include <stdio.h>
#define F_CPU 16000000UL
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#define BIT(x)	(1U<<(x))
#define SETBIT(reg,bit)		((reg) |= BIT(bit))
#define CLEARBIT(reg,bit)	((reg) &= ~BIT(bit))
#define GETBIT(reg, bit)	((reg) & BIT(bit))
#define CHECKBIT(reg, bit)	(GETBIT(reg, bit) >> (bit))
#define RETURNBIT(reg,bit)	CHECKBIT(reg,bit) == 1 ? 1 : 0
#define W 1
#define R 0

#endif /* MK_UTILITIES_H_ */