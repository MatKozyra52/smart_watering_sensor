/*
 * Sleep library file
 * Created: 11.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : ATmega328p deep-sleep mode
 */ 

#ifndef MK_SLEEP_H_
#define MK_SLEEP_H_

#include <avr/wdt.h>
#include <avr/sleep.h>
#include "MK_utilities.h"

// Watchdog interval
#define SLEEP_15MS	WDTO_15MS
#define SLEEP_30MS	WDTO_30MS
#define SLEEP_60MS	WDTO_60MS
#define SLEEP_120MS	WDTO_120MS
#define SLEEP_250MS	WDTO_250MS
#define SLEEP_500MS	WDTO_500MS
#define SLEEP_1S	WDTO_1S
#define SLEEP_2S	WDTO_2S
#define SLEEP_4S	0x20
#define SLEEP_8S	0x21

#define Reset_AVR() wdt_enable(WDTO_30MS); while(1) {}
	
void SLEEP_watchdogSetup(uint8_t value);
void SLEEP_forMin(int min);
void SLEEP_toIrq(uint8_t value);

#endif /* MK_SLEEP_H_ */