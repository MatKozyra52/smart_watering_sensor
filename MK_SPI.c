#include "MK_SPI.h"
#include "nrf24l01-mnemonics.h"
#include <stdlib.h>
/* Private definitions -------------------------------------------------------*/

/* Definition for SPI Pins */
#define SPI_DDR		DDRB
#define SPI_PORT	PORTB
#define CSN			PINB2
#define MOSI		PINB3
#define MISO		PINB4
#define SCK			PINB5


/* Device properties */
#define WRITE_OFFSET	0x20
/*-----------------------------------------------------------------------------*/

//SPI Macro
#define START_SPI_TRANSMISSION	CLEARBIT(SPI_PORT,CSN)
#define STOP_SPI_TRANSMISSION	SETBIT(SPI_PORT, CSN)




void SPIinit(void){
	//CSN, MOSI and SCK -> output      MISO -> input
	SPI_DDR |= (1 << CSN) | (1 << MOSI) | (1 << SCK);
	
	/* Enable SPI, Master, set clock rate fck/16 */
	SPCR =	(1<<SPE)|
			(1<<MSTR)|
			(1<<SPR0);
	
	STOP_SPI_TRANSMISSION;
	
}


void SPIwriteByte(uint8_t dataByte){
	/* Data => SPI Data Register */
	/* Wait for transmission complete -- flag value set */
	SPDR = dataByte;
	while(!(SPSR & (1 << SPIF)));
}

uint8_t SPIreadByte(void){
	// transmit dummy byte to enable clock for slave
	// Wait for reception complete
	// return Data Register
	
	SPDR = 0xFF;
	while(!(SPSR & (1 << SPIF)));
	return SPDR;
}

uint8_t SPIreadWrite(uint8_t dataByte)
{
	/* Data => SPI Data Register */
	// Wait for reception complete
	// return Data Register
	
	SPDR = dataByte;
	while(!(SPSR & (1 << SPIF)));	
	return SPDR;
}

void SPIgetReg(uint8_t regID, uint8_t *dataByte, uint8_t dataSize){
	
	START_SPI_TRANSMISSION;
	_delay_us(10);
	SPIwriteByte(regID);
	for (uint8_t k = 0; k<dataSize; k++)
	{
		_delay_us(10);
		dataByte[k] = SPIreadByte();
	}
	_delay_us(10);
	STOP_SPI_TRANSMISSION;
}

void SPIwriteReg(uint8_t regID, uint8_t *dataByte, uint8_t dataSize){
	
	uint8_t regDest;
	_delay_us(10);
	START_SPI_TRANSMISSION;
	_delay_us(10);
	regDest = regID | WRITE_OFFSET;
	SPIwriteByte(regDest);
	for (size_t i=0; i<dataSize; i++){
		_delay_us(10);
		SPIwriteByte(dataByte[i]);
	}
	_delay_us(10);
	STOP_SPI_TRANSMISSION;
	
}

