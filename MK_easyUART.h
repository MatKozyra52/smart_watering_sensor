/*
 * UART library file
 * Created: 08.2021
 * Features : UART control for debbuging
 */ 


#ifndef MK_EASYUART_H_
#define MK_EASYUART_H_

#include "MK_utilities.h"

#define USE_PRINTF	1		// enable UART output by printf()

void initeasyUART(void);

void easyUARTsend(char byte);

void easyUARTsendString(char* string);

int printPrintf(char character, FILE *stream);

#endif /* MK_EASYUART_H_ */