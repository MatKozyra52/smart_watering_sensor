/*
 * DHT22 library file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library was developed for Smart Watering System but not used now
 * Features : DHT22 data read
 */ 

#ifndef MK_DHT22_H_ 
#define MK_DHT22_H_ 

#include "MK_utilities.h"
#include <stdbool.h>

#define DHT_DDR		DDRB
#define DHT_PORT	PORTB
#define DHT_PIN		PINB
#define DHT_DATA	0

typedef enum{
	DHT_OK,
	DHT_TIMEOUT,
	DHT_VALUE_ERROR,
	DHT_CRC_ERROR
	}DHT_STATUS;

DHT_STATUS DHT_getRaw(uint16_t *temperature, uint16_t *humidity);
DHT_STATUS DHT_getValues(int16_t *temperature, int16_t *humidity);





#endif	//MK_DHT22_H_ 