#include "MK_NRF24L01.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>

/* Private definitions -------------------------------------------------------*/

/* Definition for Pins */
// CE
#define CE_DDR		DDRB
#define CE_PORT		PORTB
#define CE_PIN		DDB1									// CE connected to PB1
// IRQ
#define IRQ_DDR		DDRD
#define IRQ_PORT	PORTD
#define IRQ_PIN		DDD2									// IRQ connected to PD2
// MOSI
#define MOSI_DDR	DDRB
#define MOSI_PORT	PORTB
#define MOSI_PIN	DDB3
// MISO
#define MISO_DDR	DDRB
#define MISO_PORT	PORTB
#define MISO_PIN	DDB4
// SCK
#define SCK_DDR		DDRB
#define SCK_PORT	PORTB
#define SCK_PIN		DDB5
/*-----------------------------------------------------------------------------*/



// Settings

#define DATARATE		RF_DR_2MBPS							// 250kbps, 1mbps, 2mbps
#define POWER			POWER_MAX							// TX power (MAX 0dBm..HIGH -6dBm..LOW -12dBm.. MIN -18dBm)
#define CHANNEL			0x74								// 2.4GHz-2.5GHz channel selection (0x01 - 0x7C)
#define RETRIES			5									// Number of retries in case of fail (0-15 range)
#define RETR_DELAY		0x0F								// Retransmission delay (0-15 range) usDelay = RETR_DELAY * 250uS + 250uS (0000� �> 250�S; �0001� �> 500�S; �1111� �> 4000�S)

// IntInterrupt source
#define RX_INTERRUPT	true								// Interrupt when message is received (RX)
#define TX_INTERRUPT	false								// Interrupt when message is sent (TX)
#define RT_INTERRUPT	false								// Interrupt when maximum re-transmits are reached (MAX_RT)


//Macro
#define INACTIVE_MODE CLEARBIT(CE_PORT,CE_PIN)
#define ACTIVE_MODE SETBIT(CE_PORT,CE_PIN)

// Used to store SPI commands
uint8_t data;



void NRF_init(void)
{	
	// CSN and CE configuration
	SETBIT(CE_DDR,CE_PIN);
	INACTIVE_MODE;
	
	// Initialize SPI
	SPIinit();
	_delay_ms(100);				// Power on reset 100ms
	
	
	// nRF24L01+ config
	data = 
	(!(RX_INTERRUPT) << MASK_RX_DR)	|	//
	(!(TX_INTERRUPT) << MASK_TX_DS) |	// Configure interupt sources
	(!(RT_INTERRUPT) << MASK_MAX_RT)|	//
	(1 << EN_CRC)					|	// EN CRC
	(1 << CRC0)						|	// 2bytes CRC lenght
	(1 << PWR_UP)					|	// Power UP
	(1 << PRIM_RX)					;	// RX mode - initial config
	SPIwriteReg(CONFIG, &data, 1);
	
	// Auto ACK
	data = 0x3F;		// Enable Auto ACK in all 6 pipes
	SPIwriteReg(EN_AA, &data, 1);
	
	// Address width
	data = 0x03;						//5 bytes address
	SPIwriteReg(SETUP_AW, &data, 1);
	
	// Retransmission
	data =
	(RETR_DELAY & 0x0F) << ARD		|	// Retransmission delay
	(RETRIES & 0x0F) << ARC			;	// Retries
	SPIwriteReg(SETUP_RETR, &data, 1);
	
	// RF Channel
	data = CHANNEL & 0x7F;				// Frequency channel
	SPIwriteReg(RF_CH, &data, 1);
	
	// Data rate
	data = 
	DATARATE						|	// Datarate 250kbps, 1Mbps, 2Mbps
	POWER							;	// Power in TX mode -18dBm - 0dBm
	SPIwriteReg(RF_SETUP, &data, 1);
	
	// RX pipe address
	//NRF_openPipe(0, default_rx_tx_addr);
	
	// TX address
	//NRF_setTxAddress(default_rx_tx_addr);
	
	// Set dynamic payload length
	data = 
	(1 << EN_DPL)					|	// Enable dynamic payload length
	(1 << EN_ACK_PAY)				;	// Dynamic ACK payload length
	SPIwriteReg(FEATURE, &data, 1);
	
	data = 0x3F;						// Dynamic payload for all pipes
	SPIwriteReg(DYNPD, &data, 1);
	
	// Clear FIFO
	NRF_flushTX();
	NRF_flushRX();
	
	// Clear interrupts
	NRF_clearInterrupt(1,1,1);
	
	// Interrupt on falling edge of INT0
	cli();
	EICRA |= (1 << ISC01);
	EIMSK |= (1 << INT0);
	sei();
}

void NRF_flushTX(){
	SPIwriteReg(FLUSH_TX,0,0);
}

void NRF_flushRX(){
	SPIwriteReg(FLUSH_RX,0,0);
}

void NRF_payloadACK(char* ack, size_t length, uint8_t pipe)
{
	SPIwriteReg(W_ACK_PAYLOAD+pipe, (uint8_t *)ack, length);
}

void NRF_setState(NRF_STATE state){
	uint8_t config_reg;
	SPIgetReg(CONFIG, &config_reg, 1);
	
	switch(state){
		case NRF_POWERUP:
		if(!(config_reg & (1 << PWR_UP))){
			SETBIT(config_reg,PWR_UP);
			SPIwriteReg(CONFIG, &config_reg, 1);
			_delay_ms(5);
		}
		break;
		///////////////////////
		
		case NRF_POWERDOWN:
		if(config_reg & (1 << PWR_UP)){
			CLEARBIT(config_reg,PWR_UP);
			SPIwriteReg(CONFIG, &config_reg, 1);
		}
		break;
		///////////////////////
		
		case NRF_PRX:
		//If power down
		if(!(config_reg & (1 << PWR_UP)))NRF_setState(NRF_POWERUP);
		SETBIT(config_reg, PRIM_RX);				// RX mode
		SPIwriteReg(CONFIG, &config_reg, 1);
		INACTIVE_MODE;
		break;
		///////////////////////
		
		case NRF_RX:
		//If power down
		if(!(config_reg & (1 << PWR_UP)))NRF_setState(NRF_POWERUP);	
		if(!(config_reg & (1 << PRIM_RX)))NRF_setState(NRF_PRX);		
		ACTIVE_MODE;								// RX Settling
		break;
		///////////////////////
		
		case NRF_PTX:
		//If power down
		if(!(config_reg & (1 << PWR_UP)))NRF_setState(NRF_POWERUP);
		CLEARBIT(config_reg, PRIM_RX);
		SPIwriteReg(CONFIG, &config_reg, 1);
		INACTIVE_MODE;
		break;
		///////////////////////
		
		case NRF_TX:
		//If power down
		if(!(config_reg & (1 << PWR_UP)))NRF_setState(NRF_POWERUP);	
		if(config_reg & (1 << PRIM_RX))NRF_setState(NRF_PTX);	
		ACTIVE_MODE;								// Pulse
		_delay_us(20);
		INACTIVE_MODE;
		break;
		///////////////////////
		
		case NRF_STANDBY1:
		//If power down
		if(!(config_reg & (1 << PWR_UP)))NRF_setState(NRF_POWERUP);		
		INACTIVE_MODE;
		break;
		///////////////////////
		
		case NRF_STANDBY2:
		//If power down
		if(!(config_reg & (1 << PWR_UP)))NRF_setState(NRF_POWERUP);
		CLEARBIT(config_reg, PRIM_RX);
		SPIwriteReg(CONFIG, &config_reg, 1);
		ACTIVE_MODE;							
		break;
	}
}

bool NRF_emptyRXbuff(void){
	uint8_t config_register;
	SPIgetReg(FIFO_STATUS,&config_register,1);
	if (config_register & (1 << RX_EMPTY)) return 1;
	return 0;
}

void NRF_enable_interrupt(uint8_t mask){
	SPIgetReg(CONFIG,&data,1);
	CLEARBIT(data,mask);
	SPIwriteReg(CONFIG,&data,1);
}

void NRF_disable_interrupt(uint8_t mask){
	SPIgetReg(CONFIG,&data,1);
	SETBIT(data,mask);
	SPIwriteReg(CONFIG,&data,1);
}

uint8_t NRF_getInterruptSource(){
	SPIgetReg(STATUS,&data,1);
	if(data & 0x70) printf("IRQ_SOURCE: %X\n\r", (data & 0x70));
	return (data & 0x70);
}

uint8_t NRF_getStatus(){
	SPIgetReg(STATUS, &data, 1);
	return data;
}

NRF_TX_RESULT NRF_sendMessageTo(uint8_t* txAddress, char* tx_buffer, size_t buff_size){
	NRF_setTxAddress(txAddress);
	NRF_openPipe(0,txAddress);
	return NRF_sendMessage(tx_buffer,buff_size);
}

NRF_TX_RESULT NRF_sendMessage(char* tx_buffer, size_t buff_size){
	
	NRF_TX_RESULT result = NRF_TX_SUCCES;
	
	// Disable interrupt on RX
	NRF_disable_interrupt(MASK_RX_DR);
	
	NRF_setState(NRF_PTX);
	NRF_flushTX();

	// Start SPI, load message into TX_PAYLOAD
	SPIwriteReg(W_TX_PAYLOAD,(uint8_t *)tx_buffer,buff_size);

	// Transmit mode
	NRF_setState(NRF_TX);

	// Wait for message to be sent (TX_DS flag raised)
	bool noInterrupt = 1;
	while (noInterrupt){
		uint8_t interrupt = NRF_getInterruptSource();
		if(interrupt & (1<<TX_DS)){
			noInterrupt = 0;
			 printf("Message sent\n\r");
		}else if(interrupt & (1<<MAX_RT)){
			noInterrupt = 0;
			printf("MAX RT interrupt\n\r");
			result = NRF_TX_MAX_RT;
		}
	}
	
	NRF_clearInterrupt(0, 1, 1);
	
	// Enable interrupt on RX
	NRF_enable_interrupt(MASK_RX_DR);
	
	// Continue listening
	NRF_setState(NRF_RX);
	
	return result;
}

size_t NRF_readMessage(uint8_t* rx_buffer){
	
	memset(rx_buffer,0,32);
	size_t rx_len = 0;
	
	SPIgetReg(R_RX_PL_WID,(uint8_t*)&rx_len,1);
	
	if(rx_len > 32){					// Packet contains errors
		SPIwriteReg(FLUSH_RX,0,0);
		return 0;
	}
	
	if (rx_len > 0) SPIgetReg(R_RX_PAYLOAD,rx_buffer,rx_len);	// Read message
	
	return rx_len;
	
}

void NRF_clearInterrupt(bool RX, bool TX, bool RT){
	data =
	(RX << RX_DR)					|	// Clear RX interrupt
	(TX << TX_DS)					|	// Clear TX interrupt
	(RT << MAX_RT)					;	// Clear RT interrupt
	SPIwriteReg(STATUS, &data, 1);
}

void NRF_setPower(uint8_t powerTx){
	SPIgetReg(RF_SETUP,&data,1);
	data &= ~POWER_MASK;
	data |= powerTx;
	printf("Power:%X\n\r",data);
	SPIwriteReg(RF_SETUP, &data, 1);
}

void NRF_setDataRate(uint8_t dataRate){
	SPIgetReg(RF_SETUP,&data,1);
	data &= ~RF_DR_MASK;
	data |= dataRate;
	printf("DR:%X\n\r",data);
	SPIwriteReg(RF_SETUP, &data, 1);	
}

bool NRF_openPipe(uint8_t pipeNumber, uint8_t* rxAddress){
	// Enable data pipe
	if (pipeNumber > 5) return 0;
		
	// RX pipe address
	uint8_t in_order[5] = {rxAddress[4], rxAddress[3], rxAddress[2], rxAddress[1], rxAddress[0]};
	switch(pipeNumber){
		case 0:
		case 1:
			SPIwriteReg(RX_ADDR_P0+pipeNumber, in_order, 5);
			break;
		case 2:
		case 3:
		case 4:
		case 5:
			SPIwriteReg(RX_ADDR_P0+pipeNumber, rxAddress, 1);
			break;
	}
	
	SPIgetReg(EN_RXADDR,&data,1);
	SETBIT(data,pipeNumber);
	SPIwriteReg(EN_RXADDR, &data, 1);
	
	return 1;

}

bool NRF_closePipe(uint8_t pipeNumber){
	// Close data pipe
	SPIgetReg(EN_RXADDR,&data,1);
	if (pipeNumber > 5) return 0;
	CLEARBIT(data,pipeNumber);
	SPIwriteReg(EN_RXADDR, &data, 1);
	return 1;
}

bool NRF_closePipesAll(){
	data = 0;
	SPIwriteReg(EN_RXADDR, &data, 1);
	return 1;
}

void NRF_setTxAddress(uint8_t* txAddress){
	uint8_t in_order[5] = {txAddress[4], txAddress[3], txAddress[2], txAddress[1], txAddress[0]};
	SPIwriteReg(TX_ADDR, in_order, 5);
	NRF_openPipe(0, txAddress);
}

void NRF_printRxAddress(uint8_t pipe){
	uint8_t tab[5];
	memset(tab,0,5);
	if(pipe < 6){
		SPIgetReg(RX_ADDR_P0+pipe, tab, 5);
		printf("RX pipe %d ADDR: %X %X %X %X %X\n\r", pipe, tab[4], tab[3], tab[2], tab[1], tab[0]);
	} else{
		printf("NO such pipe\n\r");
	}
}

void NRF_printTxAddress(){
	uint8_t tab[5];
	memset(tab,0,5);
	SPIgetReg(TX_ADDR, tab, 5);
	printf("TX ADDR: 0x%X 0x%X 0x%X 0x%X 0x%X\n\r", tab[4], tab[3], tab[2], tab[1], tab[0]);
}

void NRF_printRegister(uint8_t reg){
	SPIgetReg(reg, &data, 1);
	printf("register: 0x%X\n\r", data);
}

void NRF_printDetails(){
	printf("\n\r");
	printf("**********NRF STATUS**********\n\r");
	printf("Status ");
	NRF_printRegister(STATUS);
	NRF_printRxAddress(0);
	NRF_printRxAddress(1);
	NRF_printTxAddress();
	printf("EN_AA ");
	NRF_printRegister(EN_AA);
	printf("EN_RX_ADDR ");
	NRF_printRegister(EN_RXADDR);
	printf("Channel ");
	NRF_printRegister(RF_CH);
	printf("Setup ");
	NRF_printRegister(RF_SETUP);
	printf("Config ");
	NRF_printRegister(CONFIG);
	printf("DYNPD ");
	NRF_printRegister(DYNPD);
	printf("FEATURE ");
	NRF_printRegister(FEATURE);
	printf("\n\r");
}