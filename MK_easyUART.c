#include "MK_easyUART.h"


/* For printf */
#if USE_PRINTF
	FILE uart_str = FDEV_SETUP_STREAM(printPrintf, NULL, _FDEV_SETUP_RW);
#endif


void initeasyUART(void){
	
	unsigned int ubrr = 103;
	/* Set Baudrate  */
	UBRR0H = (ubrr>>8); // transfer the upper 8 bits to UBBR0H register.
	UBRR0L = (ubrr);    // lower 8 bits
	
	UCSR0C = 0x06;       /* Frame format: 8data, 1stop bit  */
	UCSR0B = (1<<TXEN0); /* Enable  transmitter                 */
	
	
	#if USE_PRINTF
	stdout = &uart_str;
	printf("Hello printf\n\r");
	#endif
	
}

void easyUARTsendByte(char data){
	while (!( UCSR0A & (1<<UDRE0))); /* Wait for empty transmit buffer       */
	
	/* When UDRE0 = 0,data transmisson ongoing.                         */
	/*		UDRE0 = 1,data transmisson completed.                       */
	
	UDR0  = data;
}

void easyUARTsendString(char* string){
	unsigned int i = 0;
	while(string[i] != 0)
	{
		easyUARTsendByte(string[i]);
		i++;
	}
	
	/*************************************************************************************************/
	/* Send "\n" Character */
	easyUARTsendByte('\n');
	
	/* Send "\r" Character */
	easyUARTsendByte('\r');
	/*------------------------------------------------------------------------------------------------*/
}


#if USE_PRINTF

int printPrintf(char character, FILE *stream)
{
	cli();
	while (!( UCSR0A & (1<<UDRE0))); /* Wait for empty transmit buffer       */
	sei();
	/* When UDRE0 = 0,data transmisson ongoing.                         */
	/*		UDRE0 = 1,data transmisson completed.                       */
	
	UDR0  = character;

	return 0;
}

#endif
