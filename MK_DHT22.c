﻿#include "MK_DHT22.h"
#include "MK_easyUART.h"
#include <util/delay.h>

#define DHT_RESPONSE_H	50
#define DHT_START_BIT_0	90
#define DHT_START_BIT_1	90
#define DHT_DATA_LOW	60
#define DHT_DATA_0H		40
#define	DHT_DATA_MAX	120

DHT_STATUS DHT_getRaw(uint16_t *temperature, uint16_t *humidity){
	uint16_t temp, hum;
	uint8_t checksum;
	uint8_t counter;
	DHT_STATUS result = DHT_OK;
	/*start signal
	1. pull low for at least 1ms to init transmission
	2. pull-up bus voltage and wait 20-40us for DHT response							//test shows that only 12us required
	3. DHT response (low-voltage-level for 80us and next high-voltage-level for 80us)
	4. DHT sends data 40bits (16bits(temperature)+16bits(humidity)+8bits(checksum))
		data:	'0' (50us(low) + 26-28us(high))
				'1' (50us(low) + 70us(high))
	*/
	//1step
	SETBIT(DHT_DDR, DHT_DATA);
	CLEARBIT(DHT_PORT, DHT_DATA);
	_delay_ms(2);				//at least 1ms
	
	//2step
	SETBIT(DHT_PORT, DHT_DATA);
	CLEARBIT(DHT_DDR, DHT_DATA);
	
	counter = 0;
	while(GETBIT(DHT_PIN,DHT_DATA)){
		counter++;
		_delay_us(1);
		if (counter > DHT_RESPONSE_H){
			result = DHT_TIMEOUT;
			//printf("Timeout step2 ");
			break;
		}
	}
	
	//3step
	if(result == DHT_OK){
		counter = 0;
		while(!(GETBIT(DHT_PIN, DHT_DATA))){
			counter++;
			_delay_us(1);
			if(counter > DHT_START_BIT_0){
				result = DHT_TIMEOUT;
				//printf("Timeout step3 ");
				break;
			}
		}
	}
	if(result == DHT_OK){
		counter = 0;
		while((GETBIT(DHT_PIN, DHT_DATA))){
			counter++;
			_delay_us(1);
			if(counter > DHT_START_BIT_1){
				result = DHT_TIMEOUT;
				//printf("Timeout step3_1 ");
				break;
			}
		}
	}
	
	//read hum
	hum = 0;
	if(result == DHT_OK){
		for(uint8_t pos = 0; pos<16; pos++ ){
			counter = 0;
			while(!(GETBIT(DHT_PIN, DHT_DATA))){
				counter++;
				_delay_us(1);
				if(counter > DHT_DATA_LOW){
					//printf("Timeout hum0 ");
					result = DHT_TIMEOUT;
					pos = 16;				//force exit
					break;
				}
			}
			_delay_us(DHT_DATA_0H);
			if(GETBIT(DHT_PIN, DHT_DATA)) SETBIT(hum,(15-pos));
			counter = 0;
			while((GETBIT(DHT_PIN, DHT_DATA))){
				counter++;
				_delay_us(1);
				if(counter > DHT_DATA_MAX){
					//printf("Timeout hum1 ");
					result = DHT_TIMEOUT;
					break;
				}
			}
		}
	}
	//read temp
	temp = 0;
	if(result == DHT_OK){
		for(uint8_t pos = 0; pos<16; pos++ ){
			counter = 0;
			while(!(GETBIT(DHT_PIN, DHT_DATA))){
				counter++;
				_delay_us(1);
				if(counter > DHT_DATA_LOW){
					//printf("Timeout temp0 ");
					result = DHT_TIMEOUT;
					pos = 16;				//force exit
					break;
				}
			}
			_delay_us(DHT_DATA_0H);
			if(GETBIT(DHT_PIN, DHT_DATA)) SETBIT(temp,(15-pos));
			counter = 0;
			while((GETBIT(DHT_PIN, DHT_DATA))){
				counter++;
				_delay_us(1);
				if(counter > DHT_DATA_MAX){
					//printf("Timeout temp1 ");
					result = DHT_TIMEOUT;
					break;
				}
			}
		}
	}
	//read crc
	checksum = 0;
	if(result == DHT_OK){
		for(uint8_t pos = 0; pos<8; pos++ ){
			counter = 0;
			while(!(GETBIT(DHT_PIN, DHT_DATA))){
				counter++;
				_delay_us(1);
				if(counter > DHT_DATA_LOW){
					//printf("Timeout crc0 ");
					pos = 16;				//force exit
					result = DHT_TIMEOUT;
					break;
				}
			}
			_delay_us(DHT_DATA_0H);
			if(GETBIT(DHT_PIN, DHT_DATA)) SETBIT(checksum,(7-pos));
			counter = 0;
			while((GETBIT(DHT_PIN, DHT_DATA))){
				counter++;
				_delay_us(1);
				if(counter > DHT_DATA_MAX){
					//printf("Timeout crc1 ");
					result = DHT_TIMEOUT;
					break;
				}
			}
		}
	}
	uint8_t crc = (temp >> 8) + (0xFF & temp) + (hum >> 8) + (0xFF & hum);
	if(crc != checksum) result = DHT_CRC_ERROR;
	if(result == DHT_OK){
		*temperature = temp;
		*humidity = hum;
	}else{
		*temperature = 0xFFFF;
		*humidity = 0xFFFF;
	}
	
	return result;
}

DHT_STATUS DHT_getValues(int16_t *temperature, int16_t *humidity){
	uint16_t temp, hum;
	DHT_STATUS result =	DHT_getRaw(&temp,&hum);
	*humidity = hum;
	*temperature = 0x7FFF & temp;
	uint8_t minus = 0x8000 & temp;
	if(minus){
		*temperature *= -1;
	}
	if(*humidity > 1000) result = DHT_VALUE_ERROR;
	if(*temperature > 800 || *temperature < -400) result = DHT_VALUE_ERROR;
	return result;
}
