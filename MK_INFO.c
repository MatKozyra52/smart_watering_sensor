#include "MK_INFO.h"
#include "MK_DHT22.h"
#include "MK_ADC.h"
#include "MK_easyUART.h"
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

typedef struct _DEVICE{
	bool connected;
	uint8_t nodeId;
	uint8_t nodeAddress[5];
	DATA_VALUES sensorValues;
	uint8_t sleepTime;
	uint8_t pumpId;
	time_t wakeStamp;
} DEVICE;

//CFG LED
#define LED_CFG_DDR		DDRD
#define LED_CFG_PORT	PORTD
#define LED_CFG_POS		4
	
DEVICE localDevice;
uint8_t groupAddr[5];
DEVICE eeprom_val __attribute__((section(".eeprom")));

void DATA_printDEVICE(DEVICE Device){
	printf("Connected: %d, NodeID: %d, ", Device.connected, Device.nodeId);
	printf("ADDR: %X %X %X %X %X\n\r", Device.nodeAddress[4], Device.nodeAddress[3], Device.nodeAddress[2], Device.nodeAddress[1], Device.nodeAddress[0]);
}
	
void DATA_updateSensor(){
	// DHT22
	int16_t temp, hum;
	uint8_t retries = 5;
	while(DHT_getValues(&temp, &hum) && --retries){
		_delay_ms(5);
		printf("Waiting for DHT");
	}
	if (retries){
		localDevice.sensorValues.humidity = hum;
		localDevice.sensorValues.temperature = temp;
	}else printf("DHT error\n\r");
	
	// ADC
	ADC_wake();
	localDevice.sensorValues.moisture = ADC_getValue(MOISTURE_CHN, 5);
	ADC_sleep();
}
 
bool DATA_rawToPacket(uint8_t* raw_data, size_t len, DATA_PACKET* packet){

    if (len < OVERHEAD_LEN) return 0;

    packet->node_id = raw_data[0];
    uint8_t head = raw_data[1];
    packet->header.request = (head & (1 << HEADER_REQ)) >> HEADER_REQ;
    packet->header.action = (head & (1 << HEADER_ACTION)) >> HEADER_ACTION;
    packet->header.cmd_id = head & HEADER_CMD_MASK;
    memset(packet->payload, 0, 30);
    memcpy(packet->payload, raw_data+OVERHEAD_LEN, len-OVERHEAD_LEN);
    return 1;
}

size_t DATA_packetToRaw(DATA_PACKET* packet, uint8_t** raw_data){
    
    size_t pay_len = 0;
	for(size_t k = 29; k > 0; k--){
		if(packet->payload[k] != 0){
			pay_len = k+1;
			break;
		}
	}
    size_t raw_len = pay_len + OVERHEAD_LEN + 1;
    uint8_t* raw = (uint8_t*)malloc(raw_len*sizeof(uint8_t));
    memset(raw, 0, raw_len*sizeof(uint8_t));

    raw[0]= packet->node_id;

    uint8_t head = 0;
    head |= (packet->header.request << HEADER_REQ);
    head |= (packet->header.action << HEADER_ACTION);
    head |= (packet->header.cmd_id);
    raw[1] = head;
    
    memcpy(raw+OVERHEAD_LEN, packet->payload, pay_len);
    *raw_data = raw;
    return raw_len;
}

bool DATA_action(DATA_PACKET* packet){
	DATA_HEADER head = packet->header;
	bool result = 0;
	
	switch(head.cmd_id){
		/** INIT ***********************/
		case CMD_INIT:;
		result = DATA_actionInit(packet);
		
		if(result)printf("DATA_actionInit req\n\r");
		else printf("DATA_actionInit response\n\r");
		
		return result;
		break;
		
		/** PING ***********************/
		case CMD_PING:;
		result = DATA_actionPing(packet);
		
		if(result)printf("DATA_actionPing req\n\r");	
		else printf("DATA_actionPing response\n\r");
		
		return result;
		break;
		
		/** DATA ***********************/
		case CMD_GET_DATA:;
		result = DATA_actionGetData(packet);
		
		if(result) printf("DATA_actionGetData req\n\r");
		else printf("DATA_actionGetData response\n\r");
		
		return result;
		break;
		
		/** SET_TX_ADDR ***********************/
		case CMD_SET_TX_ADDR:;
		result = DATA_actionSetTxAddr(packet);

		if(result) printf("DATA_setTxAddr req\n\r");
		else printf("DATA_setTxAddr response\n\r");
		
		return result;
		break;
		
		/** SET_NODE_ID ***********************/
		case CMD_SET_NODE_ID:;
		result = DATA_actionSetNodeId(packet);
		
		if(result) printf("DATA_actionSetNodeId req\n\r");
		else printf("DATA_actionSetNodeId response\n\r");
		
		return result;
		break;
		
		/** SLEEP ***********************/
		case CMD_GO_TO_SLEEP:;
		result = DATA_actionGoToSleep(packet);
		
		if(result) printf("DATA_actionGoToSleep req\n\r");
		else printf("DATA_actionGoToSleep response\n\r");
		
		return result;
		break;
		
		default:
		return 0;
		break;
	}
}

bool DATA_actionInit(DATA_PACKET* packet){
		
	memset(&localDevice, 0, sizeof(DEVICE));
	memcpy(&localDevice, packet->payload, sizeof(DEVICE));
	
	DATA_printDEVICE(localDevice);
	
	MACHINE_saveConfig();
	MACHINE_loadConfig();
	
	DATA_createPacketResponse(packet, 1, CMD_INIT);
	sprintf((char*)(packet->payload), "ok");
	//if request we have to response
	return 1;
}

bool DATA_actionPing(DATA_PACKET* packet){
	if(!packet->header.request) return false;

	DATA_createPacketResponse(packet, 0, CMD_PING);
	sprintf((char*)(packet->payload), "pong");
	//if request we have to response
	return 1;
}


bool DATA_actionGetData(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	DATA_updateSensor();
	DATA_createPacketResponse(packet, 0, CMD_GET_DATA);
	memcpy(packet->payload, &localDevice.sensorValues, sizeof(DATA_VALUES));
	
	//if request we have to response
	return 1;
}

bool DATA_actionSetTxAddr(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	NRF_setTxAddress(packet->payload);
	printf("address changed\n\r");
	NRF_printDetails();
	
	DATA_createPacketResponse(packet, 0, CMD_SET_TX_ADDR);
	sprintf((char*)(packet->payload), "ok");
	
	return 1;
}

bool DATA_actionSetNodeId(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	localDevice.nodeId = packet->payload[0];
	
	DATA_createPacketResponse(packet, 0, CMD_SET_NODE_ID);
	sprintf((char*)(packet->payload), "ok");
	
	return 1;
}

bool DATA_actionGoToSleep(DATA_PACKET* packet){
	if(!packet->header.request) return false;
	
	localDevice.sleepTime = packet->payload[0];
	
	DATA_createPacketResponse(packet, 0, CMD_GO_TO_SLEEP);
	sprintf((char*)(packet->payload), "ok");
	
	return 1;
}

void DATA_createPacket(DATA_PACKET* packet, bool act, uint8_t cmd, bool request){
	memset(packet, 0, sizeof(DATA_PACKET));
	
	packet->node_id = localDevice.nodeId;
	packet->header.action = act;
	packet->header.cmd_id = cmd;
	packet->header.request = request;
}

void DATA_createPacketRequest(DATA_PACKET* packet, bool act, uint8_t cmd){
	DATA_createPacket(packet, act, cmd, CMD_REQUEST);
}

void DATA_createPacketResponse(DATA_PACKET* packet, bool act, uint8_t cmd){
	DATA_createPacket(packet, act, cmd, CMD_RESPONSE);
}

NRF_TX_RESULT NRF_sendPacket(DATA_PACKET *packet){
	//SEND
	uint8_t *tx_mess;
	uint8_t len = DATA_packetToRaw(packet, &tx_mess);
	uint8_t stat = NRF_sendMessage((char*)tx_mess, len);
	printf("Sending packet");
	DATA_printPacket(packet);
	free(tx_mess);
	return stat;
}


NRF_TX_RESULT DATA_sendPing(){
	DATA_PACKET packet;
	DATA_createPacketRequest(&packet, 1, CMD_PING);
	sprintf((char*)(packet.payload), "avr ping");
	
	//SEND
	return NRF_sendPacket(&packet);
}

NRF_TX_RESULT DATA_sendInit(){
	DATA_PACKET packet;
	DATA_createPacketRequest(&packet, 1, CMD_INIT);
	
	//SEND
	return NRF_sendPacket(&packet);
}

void DATA_printPacket(DATA_PACKET* packet){
	printf("Request:%d from %d\tCMD: %X\tPayload:%s\n\r", packet->header.request, packet->node_id, packet->header.cmd_id, packet->payload);
}

/////////////////////////////// STATE MACHINE ////////////////////////////////
void MACHINE_firstInit(DATA_MACHINE *machineState){
	// nrf24 - radio TX: default
	cli();
	SETBIT(LED_CFG_DDR, LED_CFG_POS);
	NRF_closePipesAll();
	NRF_setTxAddress(default_rx_tx_addr_ch1);
	NRF_setState(NRF_RX);
	sei();
}

void MACHINE_firstRun(DATA_MACHINE *machineState){
	
	if(DATA_sendInit() != NRF_TX_SUCCES){
		LED_CFG_PORT ^= BIT(LED_CFG_POS);
		SLEEP_toIrq(SLEEP_1S);
	} else{
		SLEEP_toIrq(SLEEP_1S);
	}
	if(machineState->newRx && machineState->packetBuffer.header.cmd_id == CMD_INIT && !machineState->packetBuffer.header.request) {
		if (DATA_action(&machineState->packetBuffer)){
			//RESPONSE
			printf("DATA action - send response ");
			uint8_t stat = NRF_sendPacket(&machineState->packetBuffer);
			printf("stats:%d \n\r", stat);
			
			machineState->newRx = false;
			Reset_AVR();
		}	
	}else if(machineState->newRx && (machineState->packetBuffer.header.cmd_id != CMD_INIT || machineState->packetBuffer.header.request)){
		machineState->newRx = false;
	}
}

void MACHINE_helloInit(DATA_MACHINE *machineState){
	// nrf24 - radio
	cli();
	NRF_closePipesAll();
	NRF_setTxAddress(groupAddr);
	NRF_closePipe(0);
	NRF_openPipe(1, localDevice.nodeAddress);
	NRF_setState(NRF_RX);
	sei();
}

void MACHINE_helloRun(DATA_MACHINE *machineState){
	DATA_updateSensor();
	NRF_openPipe(0, groupAddr);
	if(DATA_sendPing() != NRF_TX_SUCCES){
		NRF_closePipe(0);
		SLEEP_toIrq(SLEEP_2S);
	}else{
		NRF_closePipe(0);
		SLEEP_toIrq(SLEEP_8S);
	}
	if (machineState->newRx){
		if(machineState->packetBuffer.header.cmd_id==CMD_PING && !machineState->packetBuffer.header.request){
			printf("HELLO - master response \r\n");
			MACHINE_changeState(S_PACKET_HANDLE,machineState);
		}
		machineState->newRx = 0;
	}
}

void MACHINE_packetHandleInit(DATA_MACHINE *machineState){
	// nrf24 - radio
	cli();
	NRF_closePipesAll();
	NRF_setTxAddress(localDevice.nodeAddress);
	NRF_setState(NRF_RX);
	sei();
}

void MACHINE_packetHandleRun(DATA_MACHINE *machineState){
	if (machineState->newRx){
		cli();
		//ACTION
		if(DATA_action(&(machineState->packetBuffer))){
			//RESPONSE
			printf("DATA action - send response ");
			uint8_t stat = NRF_sendPacket(&(machineState->packetBuffer));
			printf("stats:%d \n\r", stat);
			
			//Go To sleep
			if(CMD_GO_TO_SLEEP == machineState->packetBuffer.header.cmd_id){
				MACHINE_changeState(S_SLEEP,machineState);
			}
		}
		
		machineState->newRx = 0;
		sei();
	}else{
		_delay_ms(1000);
		printf("Packet Handle - No new rx\n\r");
		NRF_printRegister(STATUS);
		NRF_printDetails();
	}
}

void MACHINE_sleepInit(DATA_MACHINE *machineState){
	NRF_setState(NRF_POWERDOWN);
}

void MACHINE_sleepRun(DATA_MACHINE *machineState){
	printf("Sleep for %d\n\r", localDevice.sleepTime);
	SLEEP_forMin(localDevice.sleepTime);
	MACHINE_changeState(S_HELLO, machineState);
}


void MACHINE_loadConfig(){
	eeprom_busy_wait();
	eeprom_read_block(&localDevice, &eeprom_val, sizeof(DEVICE));
	if (localDevice.connected == 1){
		NRF_closePipesAll();
		NRF_setTxAddress(localDevice.nodeAddress);
		memcpy(groupAddr, localDevice.nodeAddress, 5);
		groupAddr[4] = 0;
	}
	
}

void MACHINE_saveConfig(){
	eeprom_busy_wait();
	eeprom_write_block(&localDevice, &eeprom_val, sizeof(DEVICE));
}

void MACHINE_clearConfig(){
	DEVICE empty;
	memset(&empty,0x00, sizeof(DEVICE));
	eeprom_busy_wait();
	eeprom_write_block(&empty, &eeprom_val, sizeof(DEVICE));
}

void MACHINE_initMachine(DATA_MACHINE *stateMachine){
	memset(stateMachine, 0, sizeof(DATA_MACHINE));
	memset(&localDevice, 0, sizeof(DEVICE));
	
	NRF_init();
	MACHINE_loadConfig();
	DATA_printDEVICE(localDevice);
	if (localDevice.connected != 1){
		MACHINE_changeState(S_FIRST, stateMachine);
	}else{
		MACHINE_changeState(S_HELLO, stateMachine);
	}
	
	
	
}
	
void MACHINE_changeState(MACHINE_STATE setState, DATA_MACHINE *machineState){
	switch(setState){
		case S_FIRST:
		printf("Init State\t FIRST\r\n");
		MACHINE_firstInit(machineState);
		machineState->state = setState;
		break;
		
		case S_HELLO:
		printf("Init State\t Hello\r\n");
		MACHINE_helloInit(machineState);
		machineState->state = setState;
		break;
		
		case S_PACKET_HANDLE:
		printf("Init State\t Packet Handle\n\r");
		MACHINE_packetHandleInit(machineState);
		machineState->state = setState;
		break;
		
		case S_SLEEP:
		printf("Init State\t Sleep\n\r");
		MACHINE_sleepInit(machineState);
		machineState->state = setState;
		break;
	}
}


void MACHINE_stateMachine(DATA_MACHINE *machineState){
	switch(machineState->state){
		case S_FIRST:
		printf("State\t First\n\r");
		MACHINE_firstRun(machineState);
		break;
		
		case S_HELLO:
		printf("State\t Hello\n\r");
		MACHINE_helloRun(machineState);
		break;
		
		case S_PACKET_HANDLE:
		printf("State\t Packet Handle\n\r");
		MACHINE_packetHandleRun(machineState);
		break;
		
		case S_SLEEP:
		printf("State\t Sleep\n\r");
		MACHINE_sleepRun(machineState);
		break;
	}
}