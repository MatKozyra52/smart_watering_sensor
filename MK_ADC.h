/*
 * ADC library file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : ATmega328p ADC control
 */ 

#ifndef MK_ADC_H_
#define MK_ADC_H_

#include "MK_utilities.h"

/* Pinout - data */
#define MOISTURE_CHN	0
/* Pinout - power */
#define SENSOR_POWER_DDR		DDRD
#define SENSOR_POWER_PORT		PORTD
#define SENSOR_POWER_POS		6

/************************************************************************
 @brief ADC on/off control
************************************************************************/
void ADC_init();
void ADC_sleep();
void ADC_wake();

/************************************************************************
 @brief Single measure
 @param[in] channel - ADC input
************************************************************************/
uint16_t ADC_readRaw(uint8_t channel);

/************************************************************************
 @brief Multiple measurements
 @param[in] channel - ADC input
 @param[in] samles - Amount of measurements
************************************************************************/
uint16_t ADC_getValue(uint8_t channel, uint8_t samles);

#endif /* MK_ADC_H_ */