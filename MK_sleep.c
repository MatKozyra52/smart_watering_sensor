#include "MK_sleep.h"

void SLEEP_watchdogSetup(uint8_t value){
	cli();
	wdt_reset();
	/**************************************************************
	WDCE - enable watchdog settings change
	WDE - enable MC reset by watchdog
	WDPx - watchdog timer prescaler interrupt in range <16ms; 8s>
	WDIE - watchdog interrupt enable
	***************************************************************/
	WDTCSR |= (1<<WDCE) | (1<<WDE);
	WDTCSR = (1<<WDIE) | (0<<WDE) | value;  //  interrupt interval, no system reset
	sei();
}



void SLEEP_forMin(int min){
	int loops = (min*60)/8;
	SLEEP_watchdogSetup(SLEEP_8S);
	
	for(int k = 0; k < loops; k++){
		wdt_reset();
		set_sleep_mode(SLEEP_MODE_PWR_DOWN); // choose power down mode
		//  set_sleep_mode(SLEEP_MODE_PWR_SAVE); // choose power save mode
		//  set_sleep_mode(SLEEP_MODE_STANDBY); // choose external standby power mode
		//  set_sleep_mode(SLEEP_MODE_EXT_STANDBY); // choose external standby power mode
		//  set_sleep_mode(SLEEP_MODE_IDLE); // did not work like this!
		//  set_sleep_mode(SLEEP_MODE_ADC); // choose ADC noise reduction mode
		//  sleep_bod_disable();  // optional brown-out detection switch off
		sleep_mode(); // go to sleep
	}
	wdt_disable();
}

void SLEEP_toIrq(uint8_t value){
	SLEEP_watchdogSetup(value);
	
	wdt_reset();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN); // choose power down mode
	//  set_sleep_mode(SLEEP_MODE_PWR_SAVE); // choose power save mode
	//  set_sleep_mode(SLEEP_MODE_STANDBY); // choose external standby power mode
	//  set_sleep_mode(SLEEP_MODE_EXT_STANDBY); // choose external standby power mode
	//  set_sleep_mode(SLEEP_MODE_IDLE); // did not work like this!
	//  set_sleep_mode(SLEEP_MODE_ADC); // choose ADC noise reduction mode
	//  sleep_bod_disable();  // optional brown-out detection switch off
	sleep_mode(); // go to sleep
	wdt_disable();
}