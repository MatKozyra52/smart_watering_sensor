/*
 * NRF24L01 library file
 * Created: 08.2021
 * Author : Mateusz Kozyra
 * Purpose : Part of project made at AGH UST.
 * Description : This library is part of Smart Watering System
 * Features : NRF24L01 library - config, and low level operaions
 */ 


#ifndef MK_NRF24L01_H_
#define MK_NRF24L01_H_

#include "MK_SPI.h"
#include "MK_utilities.h"
#include "nrf24l01-mnemonics.h"
#include <stdbool.h>
#include <string.h>

//	States
typedef enum {
	NRF_POWERDOWN,
	NRF_POWERUP,
	NRF_PRX,
	NRF_PTX,
	NRF_RX,
	NRF_TX,
	NRF_STANDBY1,
	NRF_STANDBY2	
	}NRF_STATE;

typedef enum {
	NRF_TX_SUCCES,
	NRF_TX_MAX_RT
	}NRF_TX_RESULT;
	

const static uint8_t default_rx_tx_addr_ch1[5] = { 0xe7, 0xe7, 0xe7, 0xe7, 0xe7 };	// Default read/ write pipe address
const static uint8_t default_rx_tx_addr_ch2[5] = { 0xc2, 0xc2, 0xc2, 0xc2, 0xc2 };	// Default read/ write pipe address


void NRF_init(void);
void NRF_setState(NRF_STATE state);
bool NRF_emptyRXbuff(void);
size_t NRF_readMessage(uint8_t* rx_buffer);
void NRF_setPower(uint8_t powerTx);
void NRF_setDataRate(uint8_t dataRate);
void NRF_payloadACK(char* ack, size_t length, uint8_t pipe);
NRF_TX_RESULT NRF_sendMessageTo(uint8_t* txAddress, char* tx_buffer, size_t buff_size);
NRF_TX_RESULT NRF_sendMessage(char* tx_buffer, size_t buff_size);
void NRF_clearInterrupt(bool RX, bool TX, bool RT);
bool NRF_openPipe(uint8_t pipeNumber, uint8_t* rxAddress);
bool NRF_closePipe(uint8_t pipeNumber);
bool NRF_closePipesAll();
void NRF_flushTX();
void NRF_flushRX();
uint8_t NRF_getInterruptSource();
uint8_t NRF_getStatus();
void NRF_setTxAddress(uint8_t* txAddress);
void NRF_printRxAddress(uint8_t pipe);
void NRF_printRegister(uint8_t reg);
void NRF_printDetails();

#endif /* MK_NRF24L01_H_ */